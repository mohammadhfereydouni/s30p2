<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Intervention\Image\ImageManager;

class ImageController extends Controller
{
    /**
     * images directory path
     */
    private $imageDir = 'images';

    /**
     * resized images path
     */
    private $resizedImagesDir = 'images/resizedImages';

    /**
     * this method returns the desired image
     * @param $imageName string name of the requested image
     * @param null $height desired height
     * @param null $width desired width
     * @return BinaryFileResponse
     */
    public function imageDownload($imageName, $height = null, $width = null)
    {
        /**
         * check whether images directory exists or not.
         * creat it if it does not exists.
         */
        if (!is_dir(Storage::path($this->imageDir))) {
            Storage::makeDirectory($this->imageDir);
        }

        /**
         * check whether resized images directory exists or not.
         * creat it if it does not exists.
         */
        if (!is_dir(Storage::path($this->resizedImagesDir))) {
            Storage::makeDirectory($this->resizedImagesDir);
        }

        $imagePath = $this->imageDir . DIRECTORY_SEPARATOR . $imageName;
        /**
         * check if the image exists
         */
        abort_unless(Storage::exists($imagePath), 404);

        /**
         * start download if no height and width is specified
         */
        if ($height === null and $width === null) {
            return response()->download(Storage::path($imagePath));
        }

        /**
         * if only height is specified then the width of image will be sam as height
         */
        if ($height !== null and $width === null) {
            $width = $height;
        }

        /**
         * check if height parameter is valid
         */
        if ($height > 150 or $height < 50) {
            abort(400, 'height must be between 50 and 150');
        }

        /**
         * check if width parameter is valid
         */
        if ($width > 150 or $width < 50) {
            abort(400, 'width must be between 50 and 150');
        }

        /**
         * define resized image name and path
         */
        $imageNameParts = explode('.', $imageName);
        $resizedImageName = $imageNameParts[0] . '_' . $height . 'x' . $width . '.' . $imageNameParts[1];
        $resizedImagePath = $this->resizedImagesDir . DIRECTORY_SEPARATOR . $resizedImageName;

        /**
         * check if the resized image exists start download
         */
        if (Storage::exists($resizedImagePath)) {
            return response()->download(Storage::path($resizedImagePath));
        }

        /**
         * resize image and start download
         */
        $this->resizeImage($imagePath, $resizedImagePath, $height, $width);
        return \response()->download(Storage::path($resizedImagePath));
    }

    /**
     * this method resizes the image and saves it.
     * @param $imagePath
     * @param $resizedImagePath
     * @param $height
     * @param $width
     */
    private function resizeImage($imagePath, $resizedImagePath, $height, $width)
    {
        $manager = new ImageManager(array('driver' => 'imagick'));
        $image = $manager->make(Storage::path($imagePath));
        $image = $image->resize($width, $height);
        $image->save(Storage::path($resizedImagePath));
    }
}
