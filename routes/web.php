<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * route for download image in arbitrary size
 */
Route::get('image/{imageName}/{height?}/{width?}', 'ImageController@imageDownload')
    ->where(['imageName' => '[a-zA-Z]+\.(png|jpg|jpeg)', 'height' => '[0-9]*', 'width' => '[0-9]*']);
